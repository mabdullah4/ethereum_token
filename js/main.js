var API_URL = "http://api.ethplorer.io/"
$(document).ready(function (e) {
    $('.get_details').click(function (e) {
        e.preventDefault();
        var token = $('#token_input').val();
        var token_regex = new RegExp('/^0x([A-Fa-f0-9]{64})$/');
        if (token.length > 10) {
            $.ajax({
                type: "get",
                url: API_URL + 'getAddressInfo/' + token,
                data: {
                    'apiKey': 'freekey',
                },
                dataType: "json",
                success: function (response) {
                    $('#response_json').text(JSON.stringify(response, null, 4));
                    setTimeout(function () {
                        $.ajax({
                            type: "get",
                            url: API_URL + "getTxInfo/" + (response['contractInfo']['transactionHash']),
                            data: {
                                'apiKey': 'freekey',
                            },
                            dataType: "json",
                            success: function (result) {
                                $('#trans_json').text(JSON.stringify(result, null, 4));
                                $.toast().reset('all');
                                $.toast({
                                    title: 'Success',
                                    text: 'Found Token Successfully',
                                    hideAfter: false,
                                    showHideTransition: 'slide',
                                    position: 'top-right'
                                });
                            },
                            error: function (error) {
                                $.toast().reset('all');
                                $.toast({
                                    title: 'Error',
                                    text: 'Error connecting Server For Transaction Details',
                                    hideAfter: false,
                                    showHideTransition: 'slide',
                                    position: 'top-right'
                                });
                                return false;
                            }
                        });
                    }, 2000)
                },
                error: function (error) {
                    $.toast().reset('all');
                    $.toast({
                        title: 'Error',
                        text: 'Error connecting Server',
                        hideAfter: false,
                        showHideTransition: 'slide',
                        position: 'top-right'
                    });
                    return false;
                }
            });
            return true;
        } else {
            $.toast().reset('all');
            $.toast({
                title: 'Error',
                text: 'Invalid Token',
                hideAfter: false,
                showHideTransition: 'slide',
                position: 'top-right'
            });
        }
    });

    $('pre code').each(function (i, block) {
        hljs.highlightBlock(block);
    });
});